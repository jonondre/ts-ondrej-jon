import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StandardItemTest {

    static StandardItem standartItem;

    @BeforeAll
    public static void createStandartItem(){
        standartItem = new StandardItem(0, "Bread", 24.90f, "Pastry", 2);
    }

    @Test
    public void callConstructor_StandardItem_objectCreated(){
        assertNotNull(standartItem);
    }

    @Test
    public void copyItem_StandardItem_objectsAreSame(){
        StandardItem copiedItem;

        copiedItem = standartItem.copy();

        assertEquals(copiedItem.getID(), standartItem.getID());
        assertEquals(copiedItem.getName(), standartItem.getName());
        assertEquals(copiedItem.getPrice(), standartItem.getPrice());
        assertEquals(copiedItem.getCategory(), standartItem.getCategory());
        assertEquals(copiedItem.getLoyaltyPoints(), standartItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "1, true",
            "2, false",
            "3, false",
            "4, false",
            "5, false"
    })
    public void equals_StandardItem_ItemsAreSame(int loyaltyPoints, boolean expected) {
            StandardItem item1 = new StandardItem(0, "Bread", 35f, "Pastry",  1);
            StandardItem item2 = new StandardItem(0, "Bread", 35f, "Pastry",  loyaltyPoints);

            assertEquals(expected, item1.equals(item2));
        }
    }

