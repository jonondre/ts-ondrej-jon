import org.junit.jupiter.api.Test;
import shop.Order;
import shop.ShoppingCart;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    static Order order;

    @Test
    public void callConstructors_Order_exceptionThrown(){
        ShoppingCart cart = new ShoppingCart();

        assertThrows(IllegalArgumentException.class, () ->
                order = new Order(null, null, null, 420)
                );

        assertThrows(IllegalArgumentException.class, () ->
                order = new Order(cart, null, null)
        );
    }

    @Test
    public void callConstructors_Order_objectsCreatedSuccessfully(){
        ShoppingCart cart = new ShoppingCart();

        order = new Order(cart, "John", "Prazska 7 Praha 12", 420);
        assertNotNull(order);
        assertNotNull(order.getCustomerName());
        assertNotNull(order.getCustomerAddress());


        order = new Order(cart, "John", "Prazska 7 Praha 12");
        assertNotNull(order);
        assertNotNull(order.getCustomerName());
        assertNotNull(order.getCustomerAddress());
    }

}
