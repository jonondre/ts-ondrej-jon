

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;
import storage.ItemStock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class ItemStockTest {

    ItemStock itemStock;

    @Test
    public void callConstructor_ItemStock_objectCreated(){
        Item mockedItem = mock(Item.class);
        itemStock = new ItemStock(mockedItem);

        assertNotNull(itemStock);
        assertNotNull(itemStock.getItem());
        assertEquals(itemStock.getCount(), 0);
    }

    @ParameterizedTest
    @CsvSource({
            "1",
            "2",
            "3",
            "4",
            "5"
    })
    public void increaseCount_ItemStock_countIncreased(int addedCount){
        Item item = new StandardItem(0, "Bread", 35f, "Pastry",  1);
        ItemStock itemStock = new ItemStock(item);
        //check that it has 0 at beginning
        assertEquals(itemStock.getCount(), 0);

        itemStock.IncreaseItemCount(addedCount);
        //check increased value
        assertEquals(itemStock.getCount(), addedCount);
    }

    @ParameterizedTest
    @CsvSource({
            "1",
            "2",
            "3",
            "4",
            "5"
    })
    public void decreaseCount_ItemStock_countDecreased(int removedCount){
        Item item = new StandardItem(0, "Bread", 35f, "Pastry",  1);
        ItemStock itemStock = new ItemStock(item);
        //set initial value to 5
        itemStock.IncreaseItemCount(5);

        //check decreased value
        itemStock.decreaseItemCount(removedCount);
        assertEquals(itemStock.getCount(), 5-removedCount);
    }

}
