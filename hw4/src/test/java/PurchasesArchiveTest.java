import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class PurchasesArchiveTest {

    PurchasesArchive purchasesArchive;
    HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive;


    @BeforeEach
    public void setUp(){

        ItemPurchaseArchiveEntry entry1 = mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry entry2 = mock(ItemPurchaseArchiveEntry.class);

        itemArchive = new HashMap<>();
        itemArchive.put(0, entry1);
        itemArchive.put(1, entry2);

        ArrayList<Order> mockedOrderArchive = mock(ArrayList.class);

        purchasesArchive = new PurchasesArchive(itemArchive, mockedOrderArchive);
    }

    @Test
    public void printItemPurchaseStatistics_PurchasesArchive_outputIsCorrect(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        purchasesArchive.printItemPurchaseStatistics();

        String expectedMessage = "ITEM PURCHASE STATISTICS:" + System.lineSeparator();
        for (HashMap.Entry<Integer, ItemPurchaseArchiveEntry> entry : itemArchive.entrySet()) {
            expectedMessage += entry.getValue() + System.lineSeparator();
        }

        assertEquals(expectedMessage, outContent.toString());

        System.setOut(originalOut);
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_PurchasesArchive_zero(){
        Item it = new StandardItem(2, "Yogurt", 14.90f, "Diary products", 0);

        assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(it));
    }

    @Test
    public void putOrderToPurchasesArchive_PurchasesArchive_orderAdded(){
        Item it1 = new StandardItem(2, "Yogurt", 14.90f, "Diary products", 0);
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, "John", "Prague");

        ArrayList<Item> orderItems = new ArrayList<>();

        orderItems.add(it1);

        order.setItems(orderItems);

        purchasesArchive.putOrderToPurchasesArchive(order);

        PrintStream originalOut = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        purchasesArchive.printItemPurchaseStatistics();

        String expectedMessage = "ITEM PURCHASE STATISTICS:" + System.lineSeparator();
        for (HashMap.Entry<Integer, ItemPurchaseArchiveEntry> entry : itemArchive.entrySet()) {
            expectedMessage += entry.getValue() + System.lineSeparator();
        }

        assertEquals(expectedMessage, outContent.toString());

        System.setOut(originalOut);
    }

    @Test
    public void checkCallingOfConstructor_PurchaseArchive_constructorCalled(){
        Item it1 = new StandardItem(2, "Yogurt", 14.90f, "Diary products", 0);

        ItemPurchaseArchiveEntry entry1 = new ItemPurchaseArchiveEntry(it1);

        assertNotNull(entry1);
        assertNotNull(entry1.getRefItem());
        assertEquals(1, entry1.getCountHowManyTimesHasBeenSold());
    }



}
