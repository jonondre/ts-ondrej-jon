import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import shop.*;
import storage.NoItemInStorage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EShopControllerTest {

    EShopController eShopController;
    static Item[] storageItems;

    @BeforeAll
    public static void setUp() throws NoItemInStorage {
        EShopController.startEShop();

        int[] itemCount = {10,10,4,5,10,2};

        storageItems = new Item[]{
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        for (int i = 0; i < storageItems.length; i++) {
            EShopController.storage.insertItems(storageItems[i], itemCount[i]);
        }
    }

    @Test
    public void processTest_EShopController_sufficientStocks(){
        //testing of removing items from storage
        try {
            //number of items that were in storage
            int number1 = EShopController.storage.getItemCount(1);
            int number2 = EShopController.storage.getItemCount(2);
            int number3 = EShopController.storage.getItemCount(3);
            int number4 = EShopController.storage.getItemCount(4);

            //remove items, that would be added to cart, from storage
            EShopController.storage.removeItems(storageItems[0], 1);
            EShopController.storage.removeItems(storageItems[1], 1);
            EShopController.storage.removeItems(storageItems[2], 1);
            EShopController.storage.removeItems(storageItems[3], 1);

            //testing if their number decreased
            assertEquals(number1 - 1, EShopController.storage.getItemCount(1));
            assertEquals(number2 - 1, EShopController.storage.getItemCount(2));
            assertEquals(number3 - 1, EShopController.storage.getItemCount(3));
            assertEquals(number4 - 1, EShopController.storage.getItemCount(4));
        } catch (NoItemInStorage e) {
            throw new RuntimeException(e);
        }

        PrintStream originalOut = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        //create and fill cart
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(storageItems[0]);
        cart.addItem(storageItems[1]);
        cart.addItem(storageItems[2]);
        cart.addItem(storageItems[3]);

        //expected message of adding the items
        String expectedMessage = "Item with ID 1 added to the shopping cart." + System.lineSeparator() +
                "Item with ID 2 added to the shopping cart." + System.lineSeparator() +
                "Item with ID 3 added to the shopping cart." + System.lineSeparator() +
                "Item with ID 4 added to the shopping cart." + System.lineSeparator();

        //test that items were added
        assertEquals(expectedMessage, outContent.toString());

        System.setOut(originalOut);

        //purchase the shopping cart
        try {
            //test if purchase statistics are empty
            outContent.reset();
            System.setOut(new PrintStream(outContent));
            expectedMessage = "ITEM PURCHASE STATISTICS:" + System.lineSeparator();

            EShopController.archive.printItemPurchaseStatistics();
            assertEquals(expectedMessage, outContent.toString());

            EShopController.purchaseShoppingCart(cart, "Libuse Novakova","Kosmonautu 25, Praha 8");

            outContent.reset();

            expectedMessage = "ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                    "ITEM  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0   LOYALTY POINTS 5   HAS BEEN SOLD 1 TIMES" + System.lineSeparator() +
                    "ITEM  Item   ID 2   NAME Dancing Panda v.3 with USB port   CATEGORY GADGETS   PRICE 6000.0   LOYALTY POINTS 10   HAS BEEN SOLD 1 TIMES" + System.lineSeparator() +
                    "ITEM  Item   ID 3   NAME Screwdriver   CATEGORY TOOLS   PRICE 200.0   LOYALTY POINTS 5   HAS BEEN SOLD 1 TIMES" + System.lineSeparator() +
                    "ITEM  Item   ID 4   NAME Star Wars Jedi buzzer   CATEGORY GADGETS   ORIGINAL PRICE 500.0    DISCOUNTED PRICE 35000.0  DISCOUNT FROM Thu Aug 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013   HAS BEEN SOLD 1 TIMES" + System.lineSeparator();

            EShopController.archive.printItemPurchaseStatistics();
            //test if items from order were added to archive
            assertEquals(expectedMessage, outContent.toString());
            System.setOut(originalOut);

        } catch (NoItemInStorage e) {
            throw new RuntimeException(e);
        }
            }

    @Test
    public void processTest_EShopController_insufficientStocks(){

        ShoppingCart cart = new ShoppingCart();
        //testing of removing items from storage
        try {
            //remove items, that would be added to cart, from storage
            EShopController.storage.removeItems(storageItems[5], 4);

            //adding items which should not run
            cart.addItem(storageItems[5]);
            cart.addItem(storageItems[5]);
            cart.addItem(storageItems[5]);
            cart.addItem(storageItems[5]);
        } catch (NoItemInStorage e) {
            assertEquals("No item in storage", e.getMessage());
        }

        PrintStream originalOut = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        //expected message for empty shopping cart
        String expectedMessage = "Error: shopping cart is empty" + System.lineSeparator();
        try {
            EShopController.purchaseShoppingCart(cart, "Libuse Novakova","Kosmonautu 25, Praha 8");
            //test that cart is empty
            assertEquals(expectedMessage, outContent.toString());

            //test that item purchase statistics are empty
            outContent.reset();

            expectedMessage = "ITEM PURCHASE STATISTICS:" + System.lineSeparator();
            EShopController.archive.printItemPurchaseStatistics();

            assertEquals(expectedMessage, outContent.toString());
            System.setOut(originalOut);
        } catch (NoItemInStorage e) {
            throw new RuntimeException(e);
        }

    }


}
