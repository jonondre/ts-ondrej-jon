package cz.cvut.fel.ts1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MathUtilTest {

  @Test
  public void testFactorial5() {
    MathUtil mathUtil = new MathUtil();

    long factorial5 = mathUtil.factorial(5);

    Assertions.assertEquals(120, factorial5);
  }

  @Test
  public void testFactorial0() {
    MathUtil mathUtil = new MathUtil();

    long factorial0 = mathUtil.factorial(0);

    Assertions.assertEquals(1, factorial0);
  }
}