package cz.cvut.fel.ts1;

public class Main {
  public static void main(String[] args) {
    MathUtil mathUtil = new MathUtil();

    System.out.println(mathUtil.factorial(5));
    System.out.println(mathUtil.factorial(10));
  }
}