import cz.cvut.fel.pjv.Inventory;
import cz.cvut.fel.pjv.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import javax.swing.*;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class InventoryTest {
    Inventory inventory;
    Logger mockedlogger = mock(Logger.class);

    @BeforeEach
    void setInventory(){
        inventory = spy(new Inventory(mockedlogger, false));
    }

    //Fills whole inventory with items.
    void fillInventory(){
        Item mockedItem = mock(Item.class);
        for(int i = 0; i < 9 ; i++){
            inventory.addToInventory(mockedItem);
        }
    }

    //Tries to add item into full inventory.
    @Test
    public void addItemToInventory_InventoryFull_itemNotAdded(){
        doNothing().when(inventory).updatePlayerHotbar();
        fillInventory();

        Item mockedItem = mock(Item.class);
        boolean itemAdded = inventory.addToInventory(mockedItem);

        assertEquals(false, itemAdded);
    }

    //Tests if item is indeed removed from inventory
    @Test
    public void removeItemFromInventory_removingItemThatIsInInventory_itemRemoved(){
        doNothing().when(inventory).updatePlayerHotbar();

        Item pickaxe = new Item(0);
        Item key = new Item(3);

        inventory.addToInventory(pickaxe);
        inventory.addToInventory(key);
        inventory.addToInventory(pickaxe);

        String inventoryBeforeRemoving = "Pickaxe" + System.lineSeparator() + "Key" + System.lineSeparator() + "Pickaxe" + System.lineSeparator();
        String resultBeforeRemoving = "";
        Item[] inv = inventory.getInventory();
        for(int i = 0; i < inv.length ; i++){
            if(inv[i] != null){
                resultBeforeRemoving += inv[i].getItemName() + System.lineSeparator();
            }
        }
        //check items before removing
        assertEquals(inventoryBeforeRemoving, resultBeforeRemoving);

        String expectedResult = "Pickaxe" + System.lineSeparator() + "Pickaxe" + System.lineSeparator();

        inventory.removeItemFromInventory("Key");

        String result = "";
        inv = inventory.getInventory();
        for(int i = 0; i < inv.length ; i++){
            if(inv[i] != null){
                result += inv[i].getItemName() + System.lineSeparator();
            }
        }
        //check items after removing
        assertEquals(expectedResult, result);
    }

    //Testing that after calling method pay money increases.
    @Test
    public void addMoney_InventoryNotFull_moneyAdded(){
        doNothing().when(inventory).updatePlayerHotbar();

        for (int i = 0; i < 200; i++){
            String[][] layout = new String[1][1];
            JLabel[][] tiles = new JLabel[1][1];

            inventory.addMoney(-1, -1, layout, tiles);
        }

        assertEquals(200, inventory.getMoneyCount());
    }

    //Test that after calling method pay money decreases.
    @Test
    public void pay_HasEnoughMoney_moneyDecreased(){
        doNothing().when(inventory).updatePlayerHotbar();

        for (int i = 0; i < 200; i++){
            String[][] layout = new String[1][1];
            JLabel[][] tiles = new JLabel[1][1];

            inventory.addMoney(-1, -1, layout, tiles);
        }

        inventory.pay(100);

        assertEquals(100, inventory.getMoneyCount());
    }

    @Test
    public void getEmptyIndex_InventoryFull_specialIndexReturned(){
        doNothing().when(inventory).updatePlayerHotbar();

        fillInventory();

        int emptyIndex = inventory.getEmptySlot();

        assertEquals(-1, emptyIndex);
    }



}
