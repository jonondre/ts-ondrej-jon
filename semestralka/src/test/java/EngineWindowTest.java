import cz.cvut.fel.pjv.EngineWindow;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

public class EngineWindowTest {

    private EngineWindow window;
    
    @BeforeEach
    public void setUp() {

        window = new EngineWindow();
    }

    @Test
    public void testTwoDoorsAndTwoPlayersShouldBeInvalidInput() {

        boolean result = window.checkFile("src/test/java/test_two_doors_and_players.txt");

        assertFalse(result, "ERROR There must be one player  and one door in level file!");

    }
    @Test
    public void correctFileLoadingTest()  {

        String input = ("src/test/java/correct_file_loading.txt");

        String[][] layout = window.getLevelLayout(input);

        assertEquals("D", layout[0][0]);
        assertEquals("#", layout[1][0]);
        assertEquals("+", layout[2][0]);

        assertEquals("+", layout[0][1]);
        assertEquals("+", layout[1][1]);
        assertEquals("+", layout[2][1]);

        assertEquals("#", layout[0][2]);
        assertEquals("#", layout[1][2]);
        assertEquals("#", layout[2][2]);
        
    }
    @Test
    public void layoutDimensionsShouldBeCorrectlyIdentified()  {

        int[] one_x_one_expected = {1,1};

        int[] one_x_one_actual = window.getLayoutDimensions("src/test/java/1x1.txt");

        int[] two_x_two_expected = {2,2};

        int[] two_x_two_actual = window.getLayoutDimensions("src/test/java/2x2.txt");

        assertArrayEquals(one_x_one_expected, one_x_one_actual);

        assertArrayEquals(two_x_two_expected, two_x_two_actual);

    }
     @Test
    public void levelUIshouldBeRemovedAfterRestart() {

        window.spawnPlayer(new String[][] { { "P" } });
        window.displayTileset(new String[][] { { "+" } });

        window.removeLevelUI();

        assertNull(window.getPlayer());
        assertTrue(window.npcList.isEmpty());



    }
}