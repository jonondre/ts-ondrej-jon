import cz.cvut.fel.pjv.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class IntegrationTests {

    CraftingTable craftingTable;

    Player player;
    Logger logger;
    JLabel[][] tilesLabels;
    String[][] layout;
    Inventory inventory;
    @BeforeEach
    void setupPlayer(){
        JPanel tilemapPanel = mock(JPanel.class);
        layout = new String[1][1];
        tilesLabels = new JLabel[1][1];
        JPanel playerPanel = new JPanel();
        ArrayList<NPC> npcList = new ArrayList<>();
        EngineWindow engineWindow = mock(EngineWindow.class);
        logger = mock(Logger.class);

        player = spy(new Player(1, 1, craftingTable, tilemapPanel, layout, tilesLabels, playerPanel, npcList, engineWindow, logger, false));

        doNothing().when(player).createInventory(false);
        inventory = spy(new Inventory(logger, false));
        player.setInventory(inventory);
    }

    @Test
    void craftPickaxe_sufficientMaterials_pickaxeCrafted(){
        doNothing().when(inventory).updatePlayerHotbar();

        //materials for pickaxe crafting added to inventory
        Item stick = new Item(4);
        Item diamond = new Item(5);
        inventory.addToInventory(stick);
        inventory.addToInventory(diamond);

        //verify that player has stick and diamond before crafting
        String expectedInventory = "Stick" + System.lineSeparator() + "Diamond" + System.lineSeparator();
        Item[] inv = inventory.getInventory();
        String actualInventory = "";
        for(int i = 0; i < inv.length; i++){
            if(inv[i] != null) {
                actualInventory += inv[i].getItemName();
                actualInventory += System.lineSeparator();
            }
        }
        assertEquals(expectedInventory, actualInventory);

        JLayeredPane jLayeredPane = new JLayeredPane();
        craftingTable = new CraftingTable(jLayeredPane);
        craftingTable.setPlayer(player);
        craftingTable.craftPickaxe();

        //verify that player has only pickaxe after crafting
        expectedInventory = "Pickaxe" + System.lineSeparator();
        inv = inventory.getInventory();
        actualInventory = "";
        for(int i = 0; i < inv.length; i++){
            if(inv[i] != null) {
                actualInventory += inv[i].getItemName();
                actualInventory += System.lineSeparator();
            }
        }
        assertEquals(expectedInventory, actualInventory);
    }

    @Test
    void buyItems_sufficientMoney_itemsBought(){
        doNothing().when(inventory).updatePlayerHotbar();

        JLayeredPane jLayeredPane = new JLayeredPane();
        //setup 3 NPCs they sell water bucket, packaxe and diamond and their prices are 5, 10 and 3
        NPC bucketNpc = new NPC(1, 1, jLayeredPane);
        bucketNpc.setNPCData(1, 5, player);
        NPC pickaxeNpc = new NPC(1, 1, jLayeredPane);
        pickaxeNpc.setNPCData(0, 10, player);
        NPC diamondNpc = new NPC(1, 1, jLayeredPane);
        diamondNpc.setNPCData(5, 3, player);

        //add 40 coins to players inventory
        for(int i = 0; i < 40; i++) {
            player.getInventory().addMoney(-1, -1, layout, tilesLabels);
        }
        int actualMoney = player.getInventory().getMoneyCount();
        //verify that player has 40 coins
        assertEquals(40, actualMoney);

        //buying 2 items of each kind - total price: 36
        bucketNpc.buyItem();
        bucketNpc.buyItem();
        pickaxeNpc.buyItem();
        pickaxeNpc.buyItem();
        diamondNpc.buyItem();
        diamondNpc.buyItem();

        String expectedInventory = "Money" + System.lineSeparator() +
                "Water_bucket" + System.lineSeparator() + "Water_bucket" + System.lineSeparator() +
                "Pickaxe" + System.lineSeparator() + "Pickaxe" + System.lineSeparator() +
                "Diamond" + System.lineSeparator() + "Diamond" + System.lineSeparator();
        String actualInventory = "";
        Item[] inv = player.getInventory().getInventory();
        for(int i = 0; i < inv.length; i++){
            if(inv[i] != null) {
                actualInventory += inv[i].getItemName();
                actualInventory += System.lineSeparator();
            }
        }
        assertEquals(expectedInventory, actualInventory);
        int[] amounts = player.getInventory().getAmounts();
        //verify that player has 4 coins left
        assertEquals(4, amounts[0]);
    }

    @Test
    void buyItem_insufficientMoney_itemNotBought(){
        doNothing().when(inventory).updatePlayerHotbar();

        JLayeredPane jLayeredPane = new JLayeredPane();
        //setup 1 NPC that sells key for 50 coins
        NPC keyNpc = new NPC(1, 1, jLayeredPane);
        keyNpc.setNPCData(3, 50, player);

        //add 49 coins to players inventory
        for(int i = 0; i < 49; i++) {
            player.getInventory().addMoney(-1, -1, layout, tilesLabels);
        }
        int actualMoney = player.getInventory().getMoneyCount();
        //verify that player has 49 coins
        assertEquals(49, actualMoney);

        //trying to buy key
        keyNpc.buyItem();

        String expectedInventory = "Money" + System.lineSeparator();
        String actualInventory = "";
        Item[] inv = player.getInventory().getInventory();
        for(int i = 0; i < inv.length; i++){
            if(inv[i] != null) {
                actualInventory += inv[i].getItemName();
                actualInventory += System.lineSeparator();
            }
        }
        assertEquals(expectedInventory, actualInventory);
        int[] amounts = player.getInventory().getAmounts();
        //verify that player has still 49 coins
        assertEquals(49, amounts[0]);
    }

}
