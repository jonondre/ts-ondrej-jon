import cz.cvut.fel.pjv.Tile;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TileTest {

    @Test
    public void correctTileFileAdress() {

        Tile Mock_tile = Mockito.mock(Tile.class);


        when(Mock_tile.getTileImageBySymbol(null)).thenReturn(null);
        when(Mock_tile.getTileImageBySymbol("1")).thenReturn(new ImageIcon("src/main/resources/1.png"));
        when(Mock_tile.getTileImageBySymbol("ž")).thenReturn(new ImageIcon("src/main/resources/ž.png"));

        assertEquals(null, Mock_tile.getTileImageBySymbol(null));
        assertEquals(new ImageIcon("src/main/resources/1.png").getDescription(), Mock_tile.getTileImageBySymbol("1").getDescription());
        assertEquals(new ImageIcon("src/main/resources/ž.png").getDescription(), Mock_tile.getTileImageBySymbol("ž").getDescription());

    }

}