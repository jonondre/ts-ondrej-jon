import cz.cvut.fel.pjv.EngineWindow;
import cz.cvut.fel.pjv.Item;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProcessTest {

    //This method tests a single pass through the application.
    // From loading the level through player movement, collecting items, interacting with NPCs, to ending the level.
    @Test
    void ProcessTest(){
        int timeBetweenKeyPresses = 100;

        EngineWindow engineWindow = new EngineWindow();;

        String pathToTestLevel = "src\\test\\java\\test_level2.txt";
        StringSelection stringSelection = new StringSelection(pathToTestLevel);
        //Relative path to test level is saved to clip board
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        Thread engineWindowThread = new Thread(() -> {
            engineWindow.chooseLevel();
        });

        Thread robotThread = new Thread(() -> {
            try {
                // Wait for 3 seconds
                Thread.sleep(3000);

                Robot bot = new Robot();

                //Bot presses Ctrl+V and inserts relative path to test level from clipboard
                bot.keyPress(KeyEvent.VK_CONTROL);
                bot.keyPress(KeyEvent.VK_V);
                bot.keyRelease(KeyEvent.VK_V);
                bot.keyRelease(KeyEvent.VK_CONTROL);
                bot.delay(1000);

                //Bot moves to 'Open' button and confirms selection
                bot.mouseMove(890, 560);
                bot.delay(500);
                bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                bot.delay(50);
                bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            } catch (AWTException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        engineWindowThread.start();
        robotThread.start();

        try {
            engineWindowThread.join();
            robotThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Verify that correct level has been selected
        assertEquals(engineWindow.levelChooser.getSelectedFile().getName(), "test_level2.txt");
        //Verify that player has not key in inventory
        assertEquals(false, engineWindow.getPlayer().getInventory().hasKey);

        try {
            Robot bot = new Robot();
            bot.delay(1500);

            Item[] inv = engineWindow.getPlayer().getInventory().getInventory();
            int[] amounts = engineWindow.getPlayer().getInventory().getAmounts();

            //check that player has empty first inventory slot
            assertEquals(null, inv[0]);
            //Player goes 3 times right and picks up coin
            for (int i = 0; i < 3; i++){
                bot.keyPress(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
            }
            //check that player now has 1 coin
            assertEquals("Money", inv[0].getItemName());
            assertEquals(amounts[0], 1);

            //player moves slightly right and up to pick up another coin
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 3; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            //check that player now has 2 coins
            assertEquals("Money", inv[0].getItemName());
            assertEquals(amounts[0], 2);

            //player moves right and picks up another coin
            bot.keyPress(KeyEvent.VK_DOWN);
            bot.delay(timeBetweenKeyPresses);
            bot.keyRelease(KeyEvent.VK_DOWN);
            bot.delay(timeBetweenKeyPresses);
            for (int i = 0; i < 7; i++){
                bot.keyPress(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
            }
            //check that player now has 3 coins
            assertEquals("Money", inv[0].getItemName());
            assertEquals(amounts[0], 3);

            //Player moves down for last coin
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
            }
            //check that player now has 4 coins
            assertEquals("Money", inv[0].getItemName());
            assertEquals(amounts[0], 4);

            //Player goes to tile where is NPC
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 5; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            //Click on buy button
            bot.mouseMove(690, 490);
            bot.delay(timeBetweenKeyPresses);
            bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            bot.delay(timeBetweenKeyPresses);
            bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            bot.delay(timeBetweenKeyPresses);
            //Verification that player has no money, but has pickaxe
            assertEquals(null, inv[0]);
            assertEquals(amounts[0], 0);
            assertEquals("Pickaxe", inv[1].getItemName());
            assertEquals(amounts[1], 1);

            //Click on 'X' button to close NPC UI
            bot.mouseMove(1000, 165);
            bot.delay(timeBetweenKeyPresses);
            bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            bot.delay(timeBetweenKeyPresses);
            bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            bot.delay(timeBetweenKeyPresses);

            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            bot.keyPress(KeyEvent.VK_DOWN);
            bot.delay(timeBetweenKeyPresses);
            bot.keyRelease(KeyEvent.VK_DOWN);
            bot.delay(timeBetweenKeyPresses);
            for (int i = 0; i < 2; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 3; i++){
                bot.keyPress(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
            }
            //Player moves right breaks block with pickaxe and picks up water bucket
            for (int i = 0; i < 6; i++){
                bot.keyPress(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
            }
            //Verification that player has no pickaxe but has picked water bucket
            assertEquals(null, inv[1]);
            assertEquals(amounts[1], 0);
            assertEquals("Water_bucket", inv[0].getItemName());
            assertEquals(amounts[0], 1);

            for (int i = 0; i < 3; i++){
                bot.keyPress(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_DOWN);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 6; i++){
                bot.keyPress(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_RIGHT);
                bot.delay(timeBetweenKeyPresses);
            }
            //verify that player has picked up stick, diamond and key
            assertEquals("Stick", inv[1].getItemName());
            assertEquals(amounts[1], 1);
            assertEquals("Diamond", inv[2].getItemName());
            assertEquals(amounts[2], 1);
            assertEquals("Key", inv[3].getItemName());
            assertEquals(amounts[3], 1);


            for (int i = 0; i < 6; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 3; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 6; i++){
                bot.keyPress(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_LEFT);
                bot.delay(timeBetweenKeyPresses);
            }
            for (int i = 0; i < 6; i++){
                bot.keyPress(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
                bot.keyRelease(KeyEvent.VK_UP);
                bot.delay(timeBetweenKeyPresses);
            }
            bot.keyPress(KeyEvent.VK_LEFT);
            bot.delay(timeBetweenKeyPresses);
            bot.keyRelease(KeyEvent.VK_LEFT);
            bot.delay(timeBetweenKeyPresses);

            //verify that UI shows level completed text
            assertEquals("Level completed! Choose next level.", engineWindow.nameLabel.getText());

            bot.delay(3000);

        } catch (AWTException e) {
            throw new RuntimeException(e);
        }


    }

}
