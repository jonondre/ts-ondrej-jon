package cz.cvut.fel.pjv;

/**
 * Represents specific item that is in player's inventory.
 */
public class Item {
    private String[] itemNames  = {"Pickaxe", "Water_bucket", "Money", "Key", "Stick", "Diamond"};
    private String[] itemIcons = {"Pickaxe.png", "Water_bucket.png", "Money.png", "Key.png", "Stick.png", "Diamond.png"};
    private String itemName;
    private String itemIcon;
    private int itemIndex;

/**
 * Constructor sets item name and icon based on their Id
 * @param itemIndex ID that specifies item type
 */
    public Item(int itemIndex){
        setName(itemIndex);
        setIcon(itemIndex);
        this.itemIndex = itemIndex;
    }

    public void setName(int itemIndex){
        this.itemName = itemNames[itemIndex];
    }

    public void setIcon(int itemIndex){
        this.itemIcon = itemIcons[itemIndex];
    }

    public String getItemName(){
        return itemName;
    }

    public int getItemIndex(){
        return itemIndex;
    }

    public String getItemIcon(){
        return itemIcon;
    }


}
