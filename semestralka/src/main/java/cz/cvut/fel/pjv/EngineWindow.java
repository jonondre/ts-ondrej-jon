package cz.cvut.fel.pjv;

import java.awt.Color;
import java.awt.Font;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Class containing game logic for loading levels and showing UI for main menu and when player completes level.
 */
public class EngineWindow {
    private JFrame jframe;
    private JButton loadLevelButton;
    private JPanel mainMenuPanel, tileMapPanel;
    private JPanel playerPanel;
    private JLabel playerLabel, errorLabel;
    public JLabel nameLabel;
    public JFileChooser levelChooser;
    private Player player;
    private JLayeredPane levelLayers;
    private String[][] levelLayout;
    private JLabel[][] tilesLabels;
    private int playerOffsetX = 570;
    private int playerOffsetY = 280;
    private CraftingTable crafting;
    public ArrayList<NPC> npcList = new ArrayList<>();

    private int[] playerSpawnCoord = new int[2];
    private boolean firstLevel = true;
    final Logger logger;

    boolean loggingEnabled = false;

    /**
     * Constructor for Engine Window class. It sets basic properties of game's UI elements like dimensions, size, color. It also displays main menu as default.
     */
    public EngineWindow() {
        logger = Logger.getLogger(EngineWindow.class.getName());

        jframe = new JFrame();
        jframe.setSize(1280, 720);
        jframe.setResizable(false);
        jframe.setLayout(null);
        jframe.getContentPane().setBackground(Color.DARK_GRAY);

        mainMenuPanel = new JPanel();
        mainMenuPanel.setBackground(Color.DARK_GRAY);
        mainMenuPanel.setLayout(null);
        mainMenuPanel.setBounds(0, 0, 1280, 720);

        loadLevelButton = new JButton("Load level");
        loadLevelButton.setFont(new Font("Arial", Font.PLAIN, 40));
        loadLevelButton.setBounds(470, 320, 300, 60);
        loadLevelButton.addActionListener(e -> {
            chooseLevel();
        });

        nameLabel = new JLabel("Cavecraft");
        nameLabel.setFont(new Font("Arial", Font.PLAIN, 55));
        nameLabel.setForeground(Color.white);
        nameLabel.setBounds(500, 20, 300, 60);

        jframe.add(mainMenuPanel);
        mainMenuPanel.add(nameLabel);
        mainMenuPanel.add(loadLevelButton);

        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        tileMapPanel = new JPanel();
        tileMapPanel.setBackground(Color.DARK_GRAY);
        tileMapPanel.setLayout(null);

        playerPanel = new JPanel();
        playerPanel.setOpaque(false);
        playerPanel.setLayout(null);
        playerPanel.setBounds(0, 0, 1280, 720);

        errorLabel = new JLabel("", SwingConstants.CENTER);
        errorLabel.setBounds(180, 100, 880, 800);
        errorLabel.setForeground(Color.white);
        errorLabel.setFont(new Font("Arial", Font.PLAIN, 30));

        mainMenuPanel.add(errorLabel);

        levelLayers = new JLayeredPane();
        crafting = new CraftingTable(levelLayers);
    }

    /**
     * Function lets user choose level file. When file is selected it checks if it is txt file and if there is 1 player and 1 door. If level file is valid it calls other functions that display level and its elements. If there is something wrong with level file information about error is written.
  */
    public void chooseLevel() {
        String projectDirectory = System.getProperty("user.dir");
        levelChooser = new JFileChooser(new File(projectDirectory));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT text files", "txt");
        levelChooser.setFileFilter(filter);

        int result = levelChooser.showOpenDialog(null);

        if (result == levelChooser.APPROVE_OPTION) {
            if (isItTextFile(levelChooser.getSelectedFile().getName())) {
                if (checkFile(levelChooser.getSelectedFile().getAbsolutePath())) {
                    levelLayout = getLevelLayout(levelChooser.getSelectedFile().getAbsolutePath());
                    tileMapPanel.setBounds(0, 0, levelLayout[0].length * 100, levelLayout.length * 100);
                    hideMainMenu();
                    displayTileset(levelLayout);
                    spawnPlayer(levelLayout);
                    setDataOfNPCS(levelChooser.getSelectedFile().getAbsolutePath());
                } else {
                    errorLabel.setText("ERROR There must be one player  and one door in level file!");
                    if (loggingEnabled) {
                        logger.warning("Level loading failed. There is invalid number of players/doors in file.");
                    }
                }
            } else {
                errorLabel.setText("ERROR Level file must be txt file!");
                if (loggingEnabled) {
                    logger.warning("File must be txt.");
                }
            }
        }
    }

    /**
     * Function goes through level file and counts how many times it finds P which represents player and D which represents door, that end level.
     * 
     * @param filePath path to file containing level
     * 
     * @return true if there is exactly 1 player and 1 door, false if
     * otherwise
     */
    public boolean checkFile(String filePath) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
            return false;
        }

        int playerCounter = 0;
        int doorCounter = 0;
        int charCode;
        try {
            while ((charCode = br.read()) != -1) {
                if (charCode == 80) {
                    playerCounter++;
                } else if (charCode == 68) {
                    doorCounter++;
                }
            }
        } catch (IOException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
        }

        if ((playerCounter < 1 || playerCounter > 1) || (doorCounter < 1 || doorCounter > 1)) {
            return false;
        }
        return true;
    }

    /**
     * This function hides UI of main menu. If it is first level it also adds player JPanel and tilemap JPanel to certain level in JLayeredPane.
     */
    private void hideMainMenu() {
        mainMenuPanel.setVisible(false);

        if (firstLevel) {
            levelLayers.setBounds(0, 0, 1280, 720);
            levelLayers.add(tileMapPanel, JLayeredPane.DEFAULT_LAYER);
            levelLayers.add(playerPanel, JLayeredPane.PALETTE_LAYER);
            jframe.add(levelLayers);
        }
        jframe.validate();
        jframe.repaint();
    }

    /**
     * Checks if name of selected file ends with '.txt'.
     * 
     * @param fileName name of selected file
     * 
     * @return true if it is txt file and false otherwise
     */
    private boolean isItTextFile(String fileName) {
        if (fileName.endsWith(".txt")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * First it get level dimensions, which is maximum width and height of given level. Then the function reads level file char by char if it finds N it creates new NPC and adds it to ArrayList of npcs. If the actual char is not P it adds it to String[][] layout. If actual char is P then + (which represents normal ground) is added to layout and player spawn coordinates are saved.
     * @param filePath file to given level file
     * @return two dimensional array of Strings, where each String represent one level tile
     */
    public String[][] getLevelLayout(String filePath) {
        int[] dimensions = getLayoutDimensions(filePath);
        int numberOfRows = dimensions[0];
        int numberOfCollums = dimensions[1];

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
            return null;
        }
        String[][] layout = new String[numberOfRows][numberOfCollums];
        String line;
        char[] characters;
        int actualRow = 0;
        int actualCollum = 0;

        try {
            while ((line = br.readLine()) != null) {
                if (line.equals("*")) {
                    break;
                }
                characters = line.toCharArray();
                for (int i = 0; i < characters.length; i++) {
                    if (!String.valueOf(characters[i]).equals("P")) {
                        if (String.valueOf(characters[i]).equals("N")) {
                            NPC npc = new NPC(actualCollum, actualRow, levelLayers);
                            npcList.add(npc);
                        }
                        if(String.valueOf(characters[i]).equals(" ")){
                            layout[actualRow][actualCollum] = null;
                        }
                        else{
                            layout[actualRow][actualCollum] = String.valueOf(characters[i]);
                        }
                    } else {
                        layout[actualRow][actualCollum] = "+";
                        playerSpawnCoord[0] = actualCollum;
                        playerSpawnCoord[1] = actualRow;
                    }
                    actualCollum++;
                }
                actualRow++;
                actualCollum = 0;
            }
        } catch (IOException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
        }
        try {
            br.close();
        } catch (IOException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
        }
        return layout;
    }

    /**
     * Goes through level file char by char and looks for maximum number of rows and longest row.
     * @param filePath path to level file
     * @return height and width of actual level
     */
    public int[] getLayoutDimensions(String filePath) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
            return null;
        }
        int lineCount = 0;
        int maxLengthOfLine = 0;
        int[] dimensions = new int[2];

        String line;
        try {
            while ((line = br.readLine()) != null) {
                if (line.equals("*")) {
                    break;
                }
                lineCount++;
                if (line.length() > maxLengthOfLine) {
                    maxLengthOfLine = line.length();
                }
            }
        } catch (IOException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
        }
        dimensions[0] = lineCount;
        dimensions[1] = maxLengthOfLine;
        try {
            br.close();
        } catch (IOException e) {
            // UPRAVIT PRED ODEVZDANIM
            e.printStackTrace();
        }
        return dimensions;
    }

    /**
     * Function goes through layout array and for each tile that is not empty (null) it creates new JLabel, that contains image of given tile. Then position of that label is set and label is added to JPanel.
     * @param levelLayout one elemet of array represents one level tile
     */
    public void displayTileset(String[][] levelLayout) {
        tilesLabels = new JLabel[levelLayout.length][levelLayout[0].length];

        int playerPosX = playerSpawnCoord[0];
        int playerPosY = playerSpawnCoord[1];

        for (int i = 0; i < levelLayout.length; i++) {
            for (int j = 0; j < levelLayout[0].length; j++) {
                if (levelLayout[i][j] != null) {
                    tilesLabels[i][j] = new JLabel(new Tile(levelLayout[i][j]).getTileImage());
                    tilesLabels[i][j].setBounds(j * 64, i * 64, 64, 64);
                    tileMapPanel.add(tilesLabels[i][j]);
                }
            }
        }
        if (loggingEnabled) {
            logger.info("Tileset displayed.");
        }
        tileMapPanel.setLocation(playerOffsetX - playerPosX * 64, playerOffsetY - playerPosY * 64);
        tileMapPanel.validate();
        tileMapPanel.repaint();
    }

    /**
     * Player is insantiated. Player icon is added to center of window. UI for inventory is added and if it is not first level items are loaded to inventory from file.
     * @param levelLayout one elemet of array represents one level tile
     */
    public void spawnPlayer(String[][] levelLayout) {
        player = new Player(playerSpawnCoord[0], playerSpawnCoord[1], crafting, tileMapPanel, levelLayout, tilesLabels,
                playerPanel, npcList,
                this, logger, loggingEnabled);
        crafting.setPlayer(player);

        if (loggingEnabled) {
            logger.info("Player spawned at coordinates x: " + player.getPlayerPosX() + " y: " + player.getPlayerPosY());
        }
        playerLabel = new JLabel(player.getPlayerIcon());
        playerLabel.setBounds(playerOffsetX, playerOffsetY, 64, 64);
        playerPanel.add(playerLabel);

        player.getInventory().showHotbar(playerPanel);
        player.getInventory().addSlots(levelLayers);
        if (!firstLevel) {
            player.getInventory().loadInventoryFromFile(player.getPlayerPosX(),
                    player.getPlayerPosY(), levelLayout, tilesLabels);
        }
        playerPanel.validate();
        playerPanel.repaint();
    }

    /**
     * Function goes through file and when it reaches '*' it starts reading NPC data. One line represents one NPC. Line is split to id of item and price, these are then passed to specific NPC.
     * @param filePath path to level file
     */
    private void setDataOfNPCS(String filePath) {
        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(filePath));

            String line;
            boolean readsNPCData = false;
            int npcIndex = 0;

            while ((line = br.readLine()) != null) {
                line = line.replace(" ", "");
                if (readsNPCData && npcIndex < npcList.size() && line != "") {
                    String[] parts = line.split(",");
                    int itemID = Integer.valueOf(parts[0]);
                    int price = Integer.valueOf(parts[1]);
                    npcList.get(npcIndex).setNPCData(itemID, price, player);
                    npcIndex++;
                }
                if (line.equals("*")) {
                    readsNPCData = true;
                }

            }
        } catch (NumberFormatException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * After reseting some variables and removing some UI it reloads current level. Tileset is displayed and player spawned as if the level was loaded for fist time.
     */
    public void restartLevel() {
        removeLevelUI();
        levelLayout = getLevelLayout(levelChooser.getSelectedFile().getAbsolutePath());
        tileMapPanel.setBounds(0, 0, levelLayout[0].length * 100, levelLayout.length * 100);
        displayTileset(levelLayout);
        spawnPlayer(levelLayout);
        setDataOfNPCS(levelChooser.getSelectedFile().getAbsolutePath());
        if (loggingEnabled) {
            logger.info("Level has been restarted.");
        }
    }

    /**
     * After reseting some variables and removing some UI it shows again main menu, but with winning text and player can select next level.
     */
    public void showLevelCompletedUI() {
        removeLevelUI();
        mainMenuPanel.setVisible(true);
        nameLabel.setText("Level completed! Choose next level.");
        nameLabel.setBounds(215, 20, 900, 60);
        if (loggingEnabled) {
            logger.info("Level completed.");
        }
        firstLevel = false;
    }

    /**
     * Player and tilemap UI elements are removed and their variables are set to null. Players inventory is removed and list of NPCs is emptied.
     */
    public void removeLevelUI() {
        levelLayout = null;
        tilesLabels = null;
        player.getInventory().removeInventoryUI(playerPanel, levelLayers);
        player.setInventory(null);
        player = null;
        playerPanel.removeAll();
        tileMapPanel.removeAll();
        npcList.clear();
    }

    public Player getPlayer(){
        return player;
    }

}
