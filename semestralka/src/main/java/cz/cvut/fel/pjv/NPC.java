package cz.cvut.fel.pjv;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * Class representing NPC, there are functions for handling buying of items and showing NPC's UI.
 */
public class NPC {
    private int posX;
    private int posY;
    private int price;
    private int itemID;
    private Player player;
    private boolean errorVisible = false;

    private JPanel NPCPanel;
    private JLabel introductionLabel, itemIconLabel, priceLabel, errorLabel;
    private JButton buyButton, closeButton;

/**
 * Constructor sets NPC's position and by default NPCs sell nothing so ID of item for sale is set to -1. It also creates its UI panel, which contains introduction text and closing button
 * @param posX x coordinates of NPC
 * @param posY y coordinates of NPC
 * @param levelLayers levelLayers contains some UI elements which should be displayed on top of other UI elements
 */
    public NPC(int posX, int posY, JLayeredPane levelLayers) {
        this.posX = posX;
        this.posY = posY;
        this.itemID = -1;

        introductionLabel = new JLabel("Hello, I have nothing for sale.");
        introductionLabel.setBounds(190, 10, 400, 80);
        introductionLabel.setFont(new Font("Arial", Font.PLAIN, 25));

        NPCPanel = new JPanel();
        NPCPanel.setBounds(250, 100, 780, 450);
        NPCPanel.setBackground(Color.LIGHT_GRAY);
        NPCPanel.setLayout(null);
        NPCPanel.add(introductionLabel);
        levelLayers.add(NPCPanel, JLayeredPane.POPUP_LAYER);
        NPCPanel.setVisible(false);

        ImageIcon closeIcon = new ImageIcon("src/main/resources/close.png");
        closeButton = new JButton(closeIcon);
        closeButton.setBackground(Color.GRAY);
        closeButton.setBounds(715, 20, 45, 45);
        NPCPanel.add(closeButton);

        setUpClosingListener();

    }

    /**
     * This function adds enlarged icon of item, that NPC is selling, to NPC panel.
     */
    private void addItemIcon(){
        Item itemForSale = new Item(itemID);

        ImageIcon itemIcon = new ImageIcon("src/main/resources/" + itemForSale.getItemIcon());
        Image originalImage = itemIcon.getImage();
        Image scaledImage = originalImage.getScaledInstance(120, 120, Image.SCALE_SMOOTH);
        itemIcon.setImage(scaledImage);
        
        itemIconLabel = new JLabel(itemIcon);
        itemIconLabel.setBounds(245, 130, 130, 130);

        NPCPanel.add(itemIconLabel);
    }

    /**
     * This function creates new JLabel, which shows how much given item costs, and adds it to NPC panel.
     */
    private void addPriceLabel(){
        priceLabel = new JLabel("Price: " + price);
        priceLabel.setBounds(395, 160, 400, 80);
        priceLabel.setFont(new Font("Arial", Font.PLAIN, 25));

        NPCPanel.add(priceLabel);
    }

    /**
     * This function adds last element that is neccessary for NPC, that is button with which player can buy items from NPC.
     */
    private void addBuyButton(){
        buyButton = new JButton();
        buyButton.setBounds(310, 330, 160, 60);
        buyButton.setBackground(Color.GRAY);

        JLabel buyLabel = new JLabel("Buy");
        buyLabel.setBounds(0, 0, 80, 60);
        buyLabel.setFont(new Font("Arial", Font.PLAIN, 20));

        buyButton.add(buyLabel);

        NPCPanel.add(buyButton);
    }

    /**
     * Based on item ID NPC's UI is set. If item ID that is in parametr is not 2 (that would mean that NPC is selling money, which doesnt make sense) then item ID variable is set to value in parameter. Also price and reference to player is set. If item ID is not -1 (default) then UI elements neccessary for buying the item are added to NPC panel.
     * @param itemID id of item that NPC sells
     * @param price how many coins given item costs
     * @param player player object
     */
    public void setNPCData(int itemID, int price, Player player){
        if(itemID != 2){
            this.itemID = itemID; 
        }
        this.price = price;
        this.player = player;

        if(this.itemID != -1){
            addItemIcon();
            addPriceLabel();
            introductionLabel.setText("Hello, I got something to sell you.");
            addBuyButton();
            addErrorLabel();
            setUpBuyingListener();
        }

    }

    /**
     * Shows NPC panel and hides error label if it was shown before.
     */
    public void showNPCUI(){
        NPCPanel.setVisible(true);
        if(errorVisible){
            hideErrorLabel();
        }
    }

    /**
     * Hides NPC panel.
     */
    public void hideNPCUI(){
        NPCPanel.setVisible(false);
    }

    /**
     * Adds label to NPC panel, this label is shown, when player does not have enough money or when his inventory is full and he wants to buy item.
     */
    private void addErrorLabel(){
        errorLabel = new JLabel("Not enough money!");
        errorLabel.setBounds(500, 320, 400, 80);
        errorLabel.setFont(new Font("Arial", Font.PLAIN, 20));

        NPCPanel.add(errorLabel);
        hideErrorLabel();
    }

    /**
     * Shows error label.
     */
    private void showErrorLabel(){
        errorLabel.setVisible(true);
        errorVisible = true;
    }

    /**
     * Hides error label.
     */
    private void hideErrorLabel(){
        errorLabel.setVisible(false);
        errorVisible = false;
    }

    /**
     * Creats and adds action listener to buying button.
     */
    public void setUpBuyingListener(){
        ActionListener buyButtonListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buyItem();
            }
        };
        buyButton.addActionListener(buyButtonListener);
    }

    /**
     * Creats and adds action listener to closing NPC panel.
     */
    private void setUpClosingListener(){
        ActionListener closeButtoListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideNPCUI();
            }
        };
        closeButton.addActionListener(closeButtoListener);
    }

    /**
     * First the button checks if player has enough money to buy item, if no then error label is shown. If player has enough money, then it is checked if item that he wants to buy can be added to inventory if yes, then it is added and amount of money is decreased. If player has exact amount money as item costs, then money is removed from inventory and that creates emtpty space for bought item to be added.
     */
    public void buyItem(){
        if(player.getInventory().getMoneyCount() >= price ){
            if(player.getInventory().addToInventory(new Item(itemID)) == true){
                player.getInventory().pay(price);
            }
            else{
                if(player.getInventory().getMoneyCount() == price){
                    player.getInventory().pay(price);
                    player.getInventory().addToInventory(new Item(itemID));
                }
                else{
                showErrorLabel();  
                errorLabel.setText("Inventory is full!");
                }
            }
        }
        else{
            showErrorLabel();
            errorLabel.setText("Not enough money!");
        }
    }

    public int getPosX(){
        return posX;
    } 

    public int getPosY(){
        return posY;
    } 
    
}
