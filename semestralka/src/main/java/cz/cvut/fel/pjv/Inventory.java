package cz.cvut.fel.pjv;

import java.awt.Color;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * Class that handles UI of players inventory and also adding, removing and finding items in player's inventory.
 */
public class Inventory {

    private Item[] inventory;
    private int[] itemAmounts;
    private JPanel playerHotbar;
    private JPanel[] inventorySlots = new JPanel[9];
    private JLabel[] itemIcons = new JLabel[9];

    private JPanel[] amountJPanels = new JPanel[9];
    private JLabel[] amountLabel = new JLabel[9];

    public boolean hasWater = false;
    public boolean hasPickaxe = false;
    public boolean hasKey = false;

    Logger logger;
    boolean loggingEnabled;

    /**
     * Constructor for Inventory class. It creates array of items and their amounts.
     * @param logger this logger prints informations/warnings when eanbled
     * @param loggingEnabled indicates if logger messages should be shown or not
     */
    public Inventory(Logger logger, boolean loggingEnabled) {
        this.inventory = new Item[9];
        this.itemAmounts = new int[9];
        this.logger = logger;
        this.loggingEnabled = loggingEnabled;
    }

    /**
     * Adds hotbar (inventory) JPanel to player's JPanel.
     * @param playerPanel UI panel containing players icon and inventory UI.
     */
    public void showHotbar(JPanel playerPanel) {
        playerHotbar = new JPanel();
        playerHotbar.setBounds(265, 605, 740, 80);
        playerHotbar.setBackground(new Color(160, 160, 160, 230));
        playerHotbar.setLayout(null);
        playerPanel.add(playerHotbar);
    }

    /**
     * This function creates 9 inventory slots. Each slot has JLabel, that will show item icon and amount panel showing number of items.
     * @param levelLayers makes sure that UI elements are displayed on correct layers
     */
    public void addSlots(JLayeredPane levelLayers) {
        int OffsetX = 20;
        int OffsetY = 10;

        for (int i = 0; i < 9; i++) {
            inventorySlots[i] = new JPanel();
            inventorySlots[i].setBounds(265 + OffsetX, 605 + OffsetY, 60, 60);
            inventorySlots[i].setBackground(new Color(243, 243, 243));

            itemIcons[i] = new JLabel();
            itemIcons[i].setBounds(265 + OffsetX, 605 + OffsetY, 60, 60);
            inventorySlots[i].add(itemIcons[i]);

            amountJPanels[i] = new JPanel();
            amountJPanels[i].setBounds(310 + OffsetX, 650 + OffsetY, 20, 18);
            amountJPanels[i].setBackground(Color.LIGHT_GRAY);

            amountLabel[i] = new JLabel("");
            amountLabel[i].setBounds(310 + OffsetX, 650 + OffsetY, 15, 15);
            amountJPanels[i].add(amountLabel[i]);
            levelLayers.add(amountJPanels[i], JLayeredPane.POPUP_LAYER);

            OffsetX += 80;
            levelLayers.add(inventorySlots[i], JLayeredPane.MODAL_LAYER);
        }
        updatePlayerHotbar();
    }

    /**
     * If inventory is not full item is added to first empty space. If added item is important information that player has it is saved.
     * @param newItem represents item that should be added to inventory
     * @return true if item was added successfully and false if not
     */
    public boolean addToInventory(Item newItem) {
        int freeIndex = getEmptySlot();
        if (freeIndex != -1) {
            if (newItem.getItemName() == "Pickaxe") {
                hasPickaxe = true;
            } else if (newItem.getItemName() == "Water_bucket") {
                hasWater = true;
            } else if (newItem.getItemName() == "Key") {
                hasKey = true;
            }
            inventory[freeIndex] = newItem;
            itemAmounts[freeIndex] = 1;
            updatePlayerHotbar();
            if(loggingEnabled){
            logger.info("Item: " + newItem.getItemName() + " added to inventory");
            }
            return true;
        }
        return false;
    }

/**
 * Function goes through players inventory and returns index of first empty slot.
 * @return index of empty slot, if all slots are occupied then it returns -1
 */
    public int getEmptySlot() {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] == null) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Function loops through items that player has in inventory and sets corresponding icons and amounts in his hotbar UI.
     */
    public void updatePlayerHotbar() {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null) {
                ImageIcon icon = new ImageIcon("src/main/resources/" + inventory[i].getItemIcon());
                Image originalImage = icon.getImage();
                Image scaledImage = originalImage.getScaledInstance(55, 55, Image.SCALE_SMOOTH);
                icon.setImage(scaledImage);
                itemIcons[i].setIcon(icon);

                amountLabel[i].setText(Integer.toString(itemAmounts[i]));
            }
            else if(inventory[i] == null && itemIcons[i].getIcon() != null || !amountLabel[i].getText().isEmpty() ){
                itemIcons[i].setIcon(null);
                amountLabel[i].setText("");
            }
        }
    }

/**
 * Looks for first appearence of given item in inventory. 
 * @param name name of wanted item
 * @return int index of wanted item or -1 if item was not found
 */
    int getItemByName(String name) {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null && inventory[i].getItemName() == name) {
                return i;
            }
        }
        return -1;
    }

    /**
     * If money should be picked up, then special function is called. Else if inventory is not full item is added to inventory and removed from map.
     * @param itemId specifies item type
     * @param posX x coordinates of item
     * @param posY y coordinates of item
     * @param levelLayout array where each element represents level tile
     * @param tilesLabels each label has (if given tile is not empty) image of tile
     */
    public void pickUpItem(int itemId, int posX, int posY, String[][] levelLayout, JLabel[][] tilesLabels) {
        if (itemId == 2) {
            addMoney(posX, posY, levelLayout, tilesLabels);
        } else if (addToInventory(new Item(itemId))) {
            removeItemFromLayout(posX, posY, levelLayout, tilesLabels);
        }
    }

    /**
     * If money is already in inventory, the its amount is increased. If it is not in inventory and inventory is not full then it is added. If position x and position y = -1 then money should be only added and layout is not changed.
     * @param posX x coordinates of coin that should be picked up
     * @param posY y coordinates of coin that should be picked up
     * @param levelLayout array where each element represents level tile
     * @param tilesLabels each label has (if given tile is not empty) image of tile
     */
    public void addMoney(int posX, int posY, String[][] levelLayout, JLabel[][] tilesLabels) {
        int itemIndex = getItemByName("Money");
        if (itemIndex != -1) {
            if(posX != -1 && posY != -1){
                removeItemFromLayout(posX, posY, levelLayout, tilesLabels);
            }
            itemAmounts[itemIndex] += 1;
            if(loggingEnabled){
            logger.info("Money picked up, amount of money has been increased to " + itemAmounts[itemIndex]);
            }
            updatePlayerHotbar();
        } else {
            if (addToInventory(new Item(2))) {
                if(posX != -1 && posY != -1){
                    removeItemFromLayout(posX, posY, levelLayout, tilesLabels);
                }
                itemIndex = getItemByName("Money");
                itemAmounts[itemIndex] = 1;
            }
        }
    }
    /**
     * Changes image of given tile to normal ground and changes it also in levelLayout.
     * @param posX x cooridantes of tile that should be changed
     * @param posY y coordinates of tile that should be changed
     * @param levelLayout array where each element represents level tile
     * @param tilesLabels each label has (if given tile is not empty) image of tile
     */
    private void removeItemFromLayout(int posX, int posY, String[][] levelLayout, JLabel[][] tilesLabels) {
        Tile tile = new Tile("+");
        tilesLabels[posY][posX].setIcon(tile.getTileImage());
        levelLayout[posY][posX] = "+";
    }

    /**
     * If given item is in inventory, then it is removed. If it was last pickaxe or water bucket then it is saved, that player doesn't have them.
     * @param itemName name of item that should be removed
     */
    public void removeItemFromInventory(String itemName) {
        int index = getItemByName(itemName);
        if (index != -1) {
            inventory[index] = null;
            itemAmounts[index] = 0;
        }
        if (getItemByName(itemName) == -1 && itemName == "Water_bucket") {
            hasWater = false;
        } else if (getItemByName(itemName) == -1 && itemName == "Pickaxe") {
            hasPickaxe = false;
        }
        updatePlayerHotbar();
    }

    /**
     * Function loops through inventory and if it finds money returns its amount.
     * @return int amount of money that player has
     */
    public int getMoneyCount() {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null && inventory[i].getItemName() == "Money") {
                return itemAmounts[i];
            }
        }
        return 0;
    }

    /**
     * The number of coins that player has is reduced and if it drops to zero, then money is removed from inventory.
     * @param payment amount of money that would be removed 
     */
    public void pay(int payment) {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null && inventory[i].getItemName() == "Money") {
                itemAmounts[i] -= payment;
                if (itemAmounts[i] == 0) {
                    removeItemFromInventory("Money");
                }
                updatePlayerHotbar();
                break;
            }
        }
        if(loggingEnabled){
        logger.info("Player paid: " + payment + " coins");
        }
    }

    /**
     * Function goes through player's inventory and if it finds item it saves its id and amount on new line in inventory.txt.
     */
    public void saveInventoryToFile() {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src/main/resources/inventory.txt"));
            for (int i = 0; i < inventory.length; i++) {
                if (inventory[i] != null) {
                    bufferedWriter.write(inventory[i].getItemIndex() + " " + itemAmounts[i]);
                    bufferedWriter.newLine();
                }
            }
            if(loggingEnabled){
            logger.info("Inventory has been saved to a file.");
            }
            bufferedWriter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Function reads inventory.txt line by line. Each line is splitted into two parts item ID and item amount. Then each item is added to inventory X times, where X is item amount.
     * @param posX x coordinates when -1 item is only added and layout is not modified 
     * @param posY y coordinates when -1 item is only added and layout is not modified 
     * @param levelLayout each element represents one level tile
     * @param tilesLabels each label (if tile != null) has image of given tile
     */
    public void loadInventoryFromFile(int posX, int posY, String[][] levelLayout, JLabel[][] tilesLabels) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/main/resources/inventory.txt"));
            for (int i = 0; i < inventory.length; i++) {
                String line = br.readLine();
                if (line != null) {
                    String[] splittedStirng = line.split(" ");
                    for (int j = 0; j < Integer.valueOf(splittedStirng[1]); j++) {
                        if(Integer.valueOf(splittedStirng[0]) == 2){
                            addMoney(posX, posY, levelLayout, tilesLabels);
                        }
                        else{
                            addToInventory(new Item(Integer.valueOf(splittedStirng[0])));
                        }
                        
                    }
                }
            }
            br.close();
            updatePlayerHotbar();
            if(loggingEnabled){
            logger.info("Loaded inventory from file.");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Children of player panel are removed as well as inventory slots and ammount panel wchich were children of JLayeredPane.
     * @param playerPanel this panel contains player icon and hotbar
     * @param levelLayers contains some UI elements which should be displayed on top of other UI elements
     */
    public void removeInventoryUI(JPanel playerPanel, JLayeredPane levelLayers){
        playerPanel.removeAll();
        for(int i = 0; i < inventorySlots.length; i++){
            levelLayers.remove(inventorySlots[i]);
            levelLayers.remove(amountJPanels[i]);
        }
    }

    public Item[] getInventory(){
        return inventory;
    }

    public int[] getAmounts(){
        return  itemAmounts;
    }

}
