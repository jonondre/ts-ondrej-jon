package cz.cvut.fel.pjv;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * In this class are functions that handle keyboard input, check if player can move in given direction and moving player. There is also function that controls tile that player steps on and if it some special tile, then it does specific action for example if player steps on tile where is some item lying on ground he picks it up.
 */
public class Player {
    private int xPosition;
    private int yPosition;
    private ImageIcon playerIcon = new ImageIcon("src/main/resources/P.png");
    private Inventory playerInventory;
    private ArrayList<NPC> npcList = new ArrayList<>();

    private CraftingTable craftingTable;
    private boolean craftingPanelVisible = false;
    private boolean NPCPanelVisible = false;
    private NPC lastNPCVisited;

    private JPanel tileMapPanel;
    private String[][] levelLayout;
    private JLabel[][] tilesLabels;
    private EngineWindow engineWindow;

    private JPanel playerPanel;

    Action leftAction, rightAction, upAction, downAction, restartAction;
    Logger logger;

    /**
     * Constructor sets variables to values in paramteres or creates new instances.
     * 
     * @param x player's x coordinates
     * 
     * @param y player's y coordinates
     * 
     * @param craftingTable object that handles crafting UI and
     * actions
     * 
     * @param tileMapPanel contains graphics of level layout
     * 
     * @param levelLayout array where each elemet represents one tile of
     * tilemap
     * 
     * @param tilesLabels array of labels where one label represents one
     * tile and has corresponding tile image
     * 
     * @param playerPanel panel containing players UI
     * 
     * @param npcList contins all NPC that are in current level
     * 
     * @param engineWindow class containing main UI and other functions
     * 
     * @param logger logs information about game state
     * 
     * @param loggingEnabled indicates if logging messages should be shown or
     * not
     */
    public Player(int x, int y, CraftingTable craftingTable, JPanel tileMapPanel, String[][] levelLayout,
            JLabel[][] tilesLabels, JPanel playerPanel, ArrayList<NPC> npcList, EngineWindow engineWindow,
            Logger logger, boolean loggingEnabled) {
        this.xPosition = x;
        this.yPosition = y;
        this.craftingTable = craftingTable;
        this.tileMapPanel = tileMapPanel;
        this.levelLayout = levelLayout;
        this.tilesLabels = tilesLabels;
        this.playerPanel = playerPanel;
        this.npcList = npcList;
        this.engineWindow = engineWindow;
        this.logger = logger;
        createInventory(loggingEnabled);
        setKeyBindings();
    }

    public void createInventory(boolean loggingEnabled){
        playerInventory = new Inventory(logger, loggingEnabled);
    }

    public ImageIcon getPlayerIcon() {
        return playerIcon;
    }

    public int getPlayerPosX() {
        return this.xPosition;
    }

    public int getPlayerPosY() {
        return this.yPosition;
    }

    public void setPlayerPos(int x, int y) {
        this.xPosition = x;
        this.yPosition = y;
    }

    /**
     * Function checks if player can move in given direction. First it checks that
     * player doesn't want to move outside of level layout or on empty tile. Then it
     * checks if player wants to move on tile that is not represented by '#' and 'O'
     * which is lava and obsidian. If player wants to move on lava tile he has to
     * have water bucket. If player wants to move on obsidian tile he has to have
     * pickaxe. If player has water bucket or pickaxe and moves on lava tile or
     * obsidian then the item is removed from inventory and tile that he stepped on
     * is replace with normal ground.
     * 
     * @param direction contains direction in which player wants to move
     * 
     * @return true if player can move in given direction otherwise it
     * returns false
     */
    private boolean checkMoveValidity(String direction) {
        int x = getPlayerPosX();
        int y = getPlayerPosY();
        switch (direction) {
            case "left":
                if (x - 1 >= 0 && levelLayout[y][x - 1] != null) {
                    if (!levelLayout[y][x - 1].equals("#")
                            && !levelLayout[y][x - 1].equals("O")) {
                        return true;
                    } else if (levelLayout[y][x - 1].equals("#") && playerInventory.hasWater) {
                        replaceTile(x - 1, y);
                        playerInventory.removeItemFromInventory("Water_bucket");
                        return true;
                    } else if (levelLayout[y][x - 1].equals("O") && playerInventory.hasPickaxe) {
                        replaceTile(x - 1, y);
                        playerInventory.removeItemFromInventory("Pickaxe");
                        return true;
                    }
                }
                return false;
            case "right":
                if (x + 1 < levelLayout[y].length && levelLayout[y][x + 1] != null) {
                    if (!levelLayout[y][x + 1].equals("#")
                            && !levelLayout[y][x + 1].equals("O")) {
                        return true;
                    } else if (levelLayout[y][x + 1].equals("#") && playerInventory.hasWater) {
                        replaceTile(x + 1, y);
                        playerInventory.removeItemFromInventory("Water_bucket");
                        return true;
                    } else if (levelLayout[y][x + 1].equals("O") && playerInventory.hasPickaxe) {
                        replaceTile(x + 1, y);
                        playerInventory.removeItemFromInventory("Pickaxe");
                        return true;
                    }
                }
                return false;
            case "up":
                if (y - 1 >= 0 && levelLayout[y - 1][x] != null) {
                    if (!levelLayout[y - 1][x].equals("#")
                            && !levelLayout[y - 1][x].equals("O")) {
                        return true;
                    } else if (levelLayout[y - 1][x].equals("#") && playerInventory.hasWater) {
                        replaceTile(x, y - 1);
                        playerInventory.removeItemFromInventory("Water_bucket");
                        return true;
                    } else if (levelLayout[y - 1][x].equals("O") && playerInventory.hasPickaxe) {
                        replaceTile(x, y - 1);
                        playerInventory.removeItemFromInventory("Pickaxe");
                        return true;
                    }
                }
                return false;
            case "down":
                if (y + 1 < levelLayout.length && levelLayout[y + 1][x] != null) {
                    if (!levelLayout[y + 1][x].equals("#")
                            && !levelLayout[y + 1][x].equals("O")) {
                        return true;
                    } else if (levelLayout[y + 1][x].equals("#") && playerInventory.hasWater) {
                        replaceTile(x, y + 1);
                        playerInventory.removeItemFromInventory("Water_bucket");
                        return true;
                    } else if (levelLayout[y + 1][x].equals("O") && playerInventory.hasPickaxe) {
                        replaceTile(x, y + 1);
                        playerInventory.removeItemFromInventory("Pickaxe");
                        return true;
                    }
                }
                return false;
        }
        return false;
    }

    /**
     * Checks tile that player is standing on. If it is number from 0 to 5 then
     * player picks up that item. If tile is represented by C UI of crafting table
     * is shown and if player has enough ingredients to craft pickaxe then
     * corresponding button is shown. If player is standing on tile represented by N
     * then it is found NPC from NPC list that has same position as player and its
     * UI is shown. If player stand on D tile and has key then his inventory is save
     * to file and level is completed.
     */
    private void checkForSpecialTiles() {
        int x = getPlayerPosX();
        int y = getPlayerPosY();

        if (craftingPanelVisible) {
            craftingTable.hideCraftingMenu();
            craftingPanelVisible = false;
        } else if (NPCPanelVisible) {
            lastNPCVisited.hideNPCUI();
            ;
            NPCPanelVisible = false;
        }

        String actualTile = levelLayout[y][x];
        switch (actualTile) {
            case "0":
                playerInventory.pickUpItem(0, x, y, levelLayout, tilesLabels);
                playerInventory.hasPickaxe = true;
                break;
            case "1":
                playerInventory.pickUpItem(1, x, y, levelLayout, tilesLabels);
                playerInventory.hasWater = true;
                break;
            case "2":
                playerInventory.pickUpItem(2, x, y, levelLayout, tilesLabels);
                break;
            case "3":
                playerInventory.pickUpItem(3, x, y, levelLayout, tilesLabels);
                break;
            case "4":
                playerInventory.pickUpItem(4, x, y, levelLayout, tilesLabels);
                break;
            case "5":
                playerInventory.pickUpItem(5, x, y, levelLayout, tilesLabels);
                break;
            case "C":
                craftingTable.showCraftingMenu();
                craftingPanelVisible = true;
                if (playerInventory.getItemByName("Stick") != -1 && playerInventory.getItemByName("Diamond") != -1) {
                    craftingTable.showPickaxeButton();
                }
                break;
            case "N":
                for (int i = 0; i < npcList.size(); i++) {
                    if (npcList.get(i).getPosX() == xPosition && npcList.get(i).getPosY() == yPosition) {
                        npcList.get(i).showNPCUI();
                        NPCPanelVisible = true;
                        lastNPCVisited = npcList.get(i);
                        break;
                    }
                }
                break;
            case "D":
                if (playerInventory.hasKey) {
                    playerInventory.removeItemFromInventory("Key");
                    playerInventory.saveInventoryToFile();
                    engineWindow.showLevelCompletedUI();
                    unsetKeyBindings();
                    engineWindow = null;
                }
                break;
        }

    }

    /**
     * Replaces tile on given coordintaes with normal ground by changing the image
     * and also level layout.
     * 
     * @param posX x coordinates of tile that should be changed
     * 
     * @param posY y coordinates of tile that should be changed
     */
    private void replaceTile(int posX, int posY) {
        tilesLabels[posY][posX].setIcon(new Tile("+").getTileImage());
        levelLayout[posY][posX] = "+";
    }

    public class MoveLeftAction extends AbstractAction {
        /**
         * This function checks if player can move left and if yes then tilemap panel
         * with labels that contain tile images is moved right by 64 pixels (size of one
         * tile), players coordinates are updated and the tile that player stands on is
         * checked.
         * 
         * @param e  event triggered by player
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isMoveValid;
            isMoveValid = checkMoveValidity("left");
            if (isMoveValid) {

                tileMapPanel.setLocation(tileMapPanel.getX() + 64, tileMapPanel.getY());
                setPlayerPos(xPosition - 1, yPosition);
                checkForSpecialTiles();
            }
        }
    }

    /**
     * This function checks if player can move right and if yes then tilemap panel
     * with labels that contain tile images is moved left by 64 pixels (size of one
     * tile), players coordinates are updated and the tile that player stands on is
     * checked.
     */
    public class MoveRightAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isMoveValid;
            isMoveValid = checkMoveValidity("right");
            if (isMoveValid) {
                tileMapPanel.setLocation(tileMapPanel.getX() - 64, tileMapPanel.getY());
                setPlayerPos(xPosition + 1, yPosition);
                checkForSpecialTiles();
            }
        }
    }

    /**
     * This function checks if player can move up and if yes then tilemap panel
     * with labels that contain tile images is moved down by 64 pixels (size of one
     * tile), players coordinates are updated and the tile that player stands on is
     * checked.
     */
    public class MoveUpAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isMoveValid;
            isMoveValid = checkMoveValidity("up");
            if (isMoveValid) {
                tileMapPanel.setLocation(tileMapPanel.getX(), tileMapPanel.getY() + 64);
                setPlayerPos(xPosition, yPosition - 1);
                checkForSpecialTiles();
            }
        }
    }

    /**
     * This function checks if player can move down and if yes then tilemap panel
     * with labels that contain tile images is moved up by 64 pixels (size of one
     * tile), players coordinates are updated and the tile that player stands on is
     * checked.
     */
    public class MoveDownAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isMoveValid;
            isMoveValid = checkMoveValidity("down");
            if (isMoveValid) {
                tileMapPanel.setLocation(tileMapPanel.getX(), tileMapPanel.getY() - 64);
                setPlayerPos(xPosition, yPosition + 1);
                checkForSpecialTiles();
            }
        }
    }

    public class RestartAction extends AbstractAction {
         /**
         * This function calls function that restarts level.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            engineWindow.restartLevel();
        }
    }

    /**
     * This function sets key bindings for player movements and game restart actions. It connects movement functions, restart action with specific keys on keyboard.
     */
    public void setKeyBindings() {
        leftAction = new MoveLeftAction();
        rightAction = new MoveRightAction();
        upAction = new MoveUpAction();
        downAction = new MoveDownAction();
        restartAction = new RestartAction();

        playerPanel.getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "leftAction");
        playerPanel.getActionMap().put("leftAction", leftAction);

        playerPanel.getInputMap().put(KeyStroke.getKeyStroke("RIGHT"), "rightAction");
        playerPanel.getActionMap().put("rightAction", rightAction);

        playerPanel.getInputMap().put(KeyStroke.getKeyStroke("UP"), "upAction");
        playerPanel.getActionMap().put("upAction", upAction);

        playerPanel.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "downAction");
        playerPanel.getActionMap().put("downAction", downAction);

        playerPanel.getInputMap().put(KeyStroke.getKeyStroke('r'), "restartAction");
        playerPanel.getActionMap().put("restartAction", restartAction);

        playerPanel.requestFocus();
    }

    /**
     * This function disconnects movement and restart functions from keyboard keys.
     */
    private void unsetKeyBindings() {
        playerPanel.getInputMap().remove(KeyStroke.getKeyStroke("LEFT"));
        playerPanel.getInputMap().remove(KeyStroke.getKeyStroke("RIGHT"));
        playerPanel.getInputMap().remove(KeyStroke.getKeyStroke("UP"));
        playerPanel.getInputMap().remove(KeyStroke.getKeyStroke("DOWN"));

        playerPanel.getInputMap().remove(KeyStroke.getKeyStroke('r'));
    }

    public Inventory getInventory() {
        return playerInventory;
    }

    public void setInventory(Inventory inv) {
        playerInventory = inv;
    }

}
