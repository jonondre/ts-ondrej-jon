package cz.cvut.fel.pjv;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * Class containing UI for crafting and function for crafting.
 */
public class CraftingTable {
    JPanel craftingPanel;
    JButton closeButton;

    JButton recipe1Button;
    Player player;

    /**
     * Constructor that creates Crafting table UI. It creates JPanel and add title "Crafting menu". It also adds button for closing and crafting. And to all buttons it adds action listeners.
     * 
     * @param levelLayers JLayeredPane, that contains level UI and graphics.
     */
    public CraftingTable(JLayeredPane levelLayers) {
        JLabel titleLabel = new JLabel("Crafting menu");
        titleLabel.setBounds(300, 0, 400, 80);
        titleLabel.setFont(new Font("Arial", Font.PLAIN, 30));

        craftingPanel = new JPanel();
        craftingPanel.setBounds(250, 100, 780, 450);
        craftingPanel.setBackground(Color.LIGHT_GRAY);
        craftingPanel.setLayout(null);
        craftingPanel.add(titleLabel);
        levelLayers.add(craftingPanel, JLayeredPane.POPUP_LAYER);
        craftingPanel.setVisible(false);

        ImageIcon closeIcon = new ImageIcon("src/main/resources/close.png");
        closeButton = new JButton(closeIcon);
        closeButton.setBackground(Color.GRAY);
        closeButton.setBounds(715, 20, 45, 45);
        craftingPanel.add(closeButton);

        createPickaxeButton();
        setupListeners();

        hidePickaxeButton();
    }

    /**
     * Shows crafting JPanlel.
     */
    public void showCraftingMenu() {
        craftingPanel.setVisible(true);
    }

        /**
     * Hides crafting JPanlel.
     */
    public void hideCraftingMenu() {
        craftingPanel.setVisible(false);

    }

    /**
     * This function creates UI of carfting button. It adds small images that show crafting recipe: stick, plus, diamond, equals, pickaxe.
     */
    public void createPickaxeButton() {
        recipe1Button = new JButton();
        recipe1Button.setBounds(40, 80, 350, 70);
        recipe1Button.setBackground(Color.GRAY);
        recipe1Button.setLayout(null);
        craftingPanel.add(recipe1Button);

        ImageIcon it1 = new ImageIcon("src/main/resources/Stick.png");
        JLabel item1 = new JLabel(it1);
        item1.setBounds(15, 5, 40, 40);
        recipe1Button.add(item1);

        ImageIcon it2 = new ImageIcon("src/main/resources/plus.png");
        JLabel item2 = new JLabel(it2);
        item2.setBounds(50, 5, 40, 40);
        recipe1Button.add(item2);

        ImageIcon it3 = new ImageIcon("src/main/resources/Diamond.png");
        JLabel item3 = new JLabel(it3);
        item3.setBounds(95, 5, 40, 40);
        recipe1Button.add(item3);

        ImageIcon it4 = new ImageIcon("src/main/resources/equals.png");
        JLabel item4 = new JLabel(it4);
        item4.setBounds(140, 5, 40, 40);
        recipe1Button.add(item4);

        ImageIcon it5 = new ImageIcon("src/main/resources/Pickaxe.png");
        JLabel item5 = new JLabel(it5);
        item5.setBounds(185, 5, 40, 40);
        recipe1Button.add(item5);
    }

        /**
     * Shows button with which player can craft pickaxe.
     */
    public void showPickaxeButton() {
        recipe1Button.setVisible(true);
    }

    
        /**
     * Hides button with which player can craft pickaxe.
     */
    public void hidePickaxeButton() {
        recipe1Button.setVisible(false);
    }

    /**
     * It adds action listeners to buttons. Close button listener hides crafting menu. Craft pickaxe listener removes ingredients from player's inventory and adds pickaxe to inventory. It also chacks if there is enough ingredients for another pickaxe and if no, then it hides the crafting button.
     */
    public void setupListeners() {
        ActionListener closeButtonListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideCraftingMenu();
            }
        };
        closeButton.addActionListener(closeButtonListener);

        ActionListener craftPickaxeListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                craftPickaxe();
            }
        };
        recipe1Button.addActionListener(craftPickaxeListener);
    }

    public void craftPickaxe(){
        player.getInventory().removeItemFromInventory("Stick");
        player.getInventory().removeItemFromInventory("Diamond");
        player.getInventory().addToInventory(new Item(0));
        if (player.getInventory().getItemByName("Stick") == -1
                || player.getInventory().getItemByName("Diamond") == -1) {
            hidePickaxeButton();
        }
    }


    public void setPlayer(Player pl) {
        player = pl;
    }
}
