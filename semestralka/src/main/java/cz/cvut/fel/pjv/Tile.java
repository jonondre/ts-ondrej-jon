package cz.cvut.fel.pjv;

import javax.swing.ImageIcon;

/**
 * Class representin specific tile in tilemap.
 */
public class Tile {
    private String tileSymbol;
    private ImageIcon tileImage;

    /**
     * Constructor that sets tile image based on symbol which represent given tile.
     * @param tileSymbol symbol of specific tile
     */
    public Tile(String tileSymbol) {
        this.tileSymbol = tileSymbol;
        this.tileImage = getTileImageBySymbol(tileSymbol);
    }

    public ImageIcon getTileImageBySymbol(String symbol){ 
        if(symbol == null){
            return null;
        }
        return new ImageIcon("src/main/resources/" + symbol + ".png");
    } 

    public ImageIcon getTileImage(){
        return tileImage;
    }
    
}
