import cz.cvut.fel.ts1.Jonondre;
import org.junit.jupiter.api.Test;
import static
org.junit.jupiter.api.Assertions.*;

public class JonondreTest {
    @Test
    public void factorialTest(){
        Jonondre jonondre = new Jonondre();
        assertEquals(1, jonondre.factorial(0));
        assertEquals(1, jonondre.factorial(1));
        assertEquals(2, jonondre.factorial(2));
        assertEquals(6, jonondre.factorial(3));
    }
}
