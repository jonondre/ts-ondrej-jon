import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    @Test
    public void calculatorAdditionTest(){
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.add(0, 1));
        assertEquals(2, calculator.add(1, 1));
        assertEquals(3, calculator.add(1, 2));
        assertEquals(4, calculator.add(2, 2));
        assertEquals(5, calculator.add(2, 3));
    }

    @Test
    public void calculatorSubtractionTest(){
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.subtract(2, 1));
        assertEquals(2, calculator.subtract(2, 0));
        assertEquals(3, calculator.subtract(5, 2));
        assertEquals(4, calculator.subtract(5, 1));
        assertEquals(5, calculator.subtract(8, 3));
    }

    @Test
    public void calculatorMultiplicationTest(){
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.multiply(1, 1));
        assertEquals(2, calculator.multiply(2, 1));
        assertEquals(3, calculator.multiply(3, 1));
        assertEquals(4, calculator.multiply(2, 2));
        assertEquals(6, calculator.multiply(2, 3));
    }

    @Test
    public void calculatorDividingTest(){
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.divide(2, 2));
        assertEquals(2, calculator.divide(8, 4));
        assertEquals(3, calculator.divide(6, 2));
        assertEquals(4, calculator.divide(12, 3));
        assertEquals(5, calculator.divide(10, 2));
    }

    @Test
    public void calculatorDividingByZeroTest(){
        Calculator calculator = new Calculator();
        assertThrows(ArithmeticException.class, () -> calculator.divide(0, 0));
        assertThrows(ArithmeticException.class, () -> calculator.divide(1, 0));
        assertThrows(ArithmeticException.class, () -> calculator.divide(2, 0));
        assertThrows(ArithmeticException.class, () -> calculator.divide(3, 0));
        assertThrows(ArithmeticException.class, () -> calculator.divide(4, 0));
    }

}
