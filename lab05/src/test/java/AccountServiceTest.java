import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class AccountServiceTest {

    @Test
    void checkBalance_mockedRepository_requestsCorrectAccount(){
        int requestedId = 1;
        BankAccount accountBank = new BankAccount(requestedId, 10);

        AccountRepository mockedRepository = mock(AccountRepository.class);
        when(mockedRepository.getById(requestedId)).thenReturn(accountBank);

        AccountService accountService = new AccountService(mockedRepository);
        accountService.checkBalance(requestedId);

        verify(mockedRepository, times(1)).getById(requestedId);
    }

    @Test
    void checkBalance_redirectedStdOutAndMockedRepository_printsCorrectSum(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        int requestedId = 1;
        int expectedBalance = 10;
        BankAccount bankAccount = new BankAccount(requestedId, expectedBalance);

        AccountRepository mockedRepository = mock(AccountRepository.class);
        when(mockedRepository.getById(requestedId)).thenReturn(bankAccount);

        AccountService accountService = new AccountService(mockedRepository);
        accountService.checkBalance(requestedId);

        assertEquals(outContent.toString(), "Account balance is: " + expectedBalance + System.lineSeparator());

        System.setOut(originalOut);
    }

    @Test
    void createNewAccount_mockedStaticNextId_nextIdCalled() {

        try (MockedStatic<AccountRepository> mockedStatic = mockStatic(AccountRepository.class)) {

            mockedStatic.when(() -> AccountRepository.nextId()).thenReturn((1));
            AccountService accountService = new AccountService(new AccountRepository());

            accountService.createNewAccount(10);

            mockedStatic.verify(() -> AccountRepository.nextId(), times(1));

        }
    }


    @Test
    void createNewAccount_mockedBankAccountConstructor_constructorCalled() {

            try(MockedConstruction<BankAccount> mockedConstruction = mockConstruction(BankAccount.class)){

                AccountService accountService = new AccountService(new AccountRepository());
                accountService.createNewAccount(10);

                assertEquals(1, mockedConstruction.constructed().size());
            }
        }




}
