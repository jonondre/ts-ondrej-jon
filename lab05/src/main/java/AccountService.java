public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void createNewAccount(int initialBalance) {
        int newId = AccountRepository.nextId();

        BankAccount bankAccount = new BankAccount(newId, initialBalance);
        accountRepository.saveAccount(bankAccount);
    }

    public void checkBalance(int id) {
        BankAccount bankAccount = accountRepository.getById(id);
        System.out.println("Account balance is: " + bankAccount.getBalance());
    }


}
