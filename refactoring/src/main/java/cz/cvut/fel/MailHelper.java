package cz.cvut.fel;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 *
 * @author balikm1
 */
public class MailHelper {
   
    public static void createAndSendMail(String to, String subject, String body)
    {
        Mail mail = getMail(to, subject, body);
        saveEmail(mail);

        sendEmailAsync(mail);
    }

    private static void sendEmailAsync(Mail mail) {
        if (!Configuration.isDebug) {
            (new Thread(() -> {
                sendMail(mail.getMailId());
            })).start();
        }
    }

    private static void saveEmail(Mail mail) {
        DBManager dbManager = new DBManager();
        dbManager.saveMail(mail);
    }

    private static Mail getMail(String to, String subject, String body) {
        Mail mail = new Mail();
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setBody(body);
        mail.setIsSent(false);
        return mail;
    }

    public static void sendMail(int mailId)
    {
        try
        {
            // get entity
            Mail mail = new DBManager().findMail(mailId);
            if (mail == null) {
                return;
            }

            if (mail.isSent()) {
                return;
            }

            MimeMessage message = getMimeMessage(mail);

            // send
            Transport.send(message);
            mail.setIsSent(true);
            new DBManager().saveMail(mail);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }

    private static MimeMessage getMimeMessage(Mail mail) throws MessagingException {
        String from = "user@fel.cvut.cz";
        String smtpHostServer = "smtp.cvut.cz";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHostServer);
        Session session = Session.getInstance(props, null);
        MimeMessage message = new MimeMessage(session);

        message.setFrom(from);
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
        message.setSubject(mail.getSubject());
        message.setText(mail.getBody(), "UTF-8");
        return message;
    }

}
